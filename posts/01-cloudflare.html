<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="icon" href="../images/favicon.png">
		<link rel="stylesheet" href="../styles/main.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>The safety of Cloudflare.</title>
	</head>
	<body>
		<header>
			<div id="header-menu" tabindex="0">
				<img
					src="../images/menu.svg"
					alt="Menu"
				>
			</div>
			<span id="header-item-list">
				<a href="index.html" class="header-item">Back to index</a>
			</span>
			<span class="header-item hide-mobile current-page">
				The safety of Cloudflare.
			</span>
			<span class="header-item">
				Tomáš Valenta - Posts
			</span>
		</header>
		<main>
			<section class="window margin-bottom">
				<h1 class="margin-bottom">
					The safety of Cloudflare.
				</h1>
				<p class="margin-bottom">
					Cloudflare is a very popular service used primarily to cache large requests for static
					content (for example, images) and protect against DDOS attacks. It's one of the pioneers
					in its field and has a largely good reputation thanks to its free services for smaller
					websites,
					<a href="https://www.cloudflare.com/ssl/" target="_blank">free TLS certificates</a>
					and privacy-enhancing tools, like <a href="https://1.1.1.1" target="_blank">1.1.1.1</a>.
				</p>
				<p>
					But is it really that good? Let's explore the possible catches.
				</p>
			</section>
			<section class="window margin-bottom">
				<h2 class="margin-bottom">
					First off, how Cloudflare works.
				</h2>
				<div class="center">
					<img
						class="margin-bottom"
						height="200"
						src="images/01-how-cloudflare-works.png"
						alt="An image showing how Cloudflare works"
					>
				</div>
				<p class="margin-bottom">
					When you request content from a website protected by Cloudflare, you're not communicating
					with it directly. Instead, you're connected to their servers which detect if you're a
					likely attacker and present you with a CAPTCHA if so. If you pass, the actual content is
					proxied through it and presented to you.
				</p>
				<p>
					This has more implications than one may realize at first.
				</p>
			</section>
			<section class="window margin-bottom">
				<h2 class="margin-bottom">
					Encrypted TLS? Not quite.
				</h2>
				<p class="margin-bottom">
					The traffic between ypi and Cloudflare's servers can be encrypted, as well as the traffic between
					their servers and the final destination. But for a brief moment, while it's being relayed and
					screened for attempts to exploit or otherwise attack the website, a decrypted version of the data
					is potentially available for any attacker, angry employee, perhaps even government agency to view
					and store.
				</p>
				<p class="margin-bottom">
					Encryption can work under the harshest of conditions when it's mathematically guaranteed,
					like regular TLS is. Cloudflare, however, introduces an element of having to trust a certain
					party not to misuse their power and to be immune to all possible attacks - even those as
					simple as bribes or an intimidating black suit man.
				</p>
				<p>
					Cloudflare is not invulnerable to exploits or the human factor. No service is. Attackers have <a
						href="https://en.wikipedia.org/wiki/Cloudbleed"
						target="_blank"
					>taken advantage</a> of this in the past, exploiting bugs and accessing millions of users'
					private information. Some of those attackers have very likely been government agencies, but,
					according to the <a
						href="https://www.cloudflare.com/resources/assets/slt3lc6tev37/7yVWG0hLy1d9627Ia6PoeJ/fb348cc38d32b88df51f72f4e100f5e6/Transparency-Report-H2-2020.pdf"
						target="_blank"
					>annual transparency report</a>, they have never obtained access to it in a legal manner.
					Whether or not that's false and Cloudflare has been ordered to withhold information about it
					is up for debate, but one which is well past the border of speculation I don't want to cross.
					They have, however, published and responded to requests for metadata, which brings us right
					to the next point.
				</p>
			</section>
			<section class="window margin-bottom">
				<h2 class="margin-bottom">
					The giant pile of metadata.
				</h2>
				<p class="margin-bottom">
					Whilst storing decrypted traffic (which may be up to a few megabytes per request) for any longer
					than necessary would be immensely wasteful, the same can't be said for a few bytes of metadata -
					for example, your IP address, device and browser. If you're presented with the typical
					<i>One more step</i> screen, a lot more information - like device identifiers, screen resolution
					and the fonts you have enabled - can also be collected and associated with you. This can be used
					to uniquely identify you across all websites using Cloudflare. It is knowingly
					<a href="https://www.cloudflare.com/privacypolicy/" target="_blank">stored</a> and available
					to website administrators in an anonymized manner for anyltics. If need-be, law enforcement can
					and will be able to get the original data.
				</p>
				<p class="margin-bottom">
					Metadata doesn't contain passwords - it doesn't need to. If there is enough of it availble, it
					reveals much more about you than a password.
				</p>
				<div class="center">
					<img
						height="325"
						src="images/01-metadata.png"
						alt="An example of metadata"
					>
				</div>
			</section>
			<section class="window margin-bottom">
				<h2 class="margin-bottom">
					The Jabbascripts.
				</h2>
				<p class="margin-bottom">
					Over 99% of all web users have it enabled. Surely it would be fine to assume every visitor
					does, right?
				</p>
				<p class="margin-bottom">
					Unless the subject is some CSS property introduced to Internet Explorer decades ago, I
					disagree. Cloudflare doesn't, and requires it to be enabled in order to get past its
					security challenges. Tor users will know this all too well, as every other popular website
					will require them to do so.
				</p>
				<p class="margin-bottom">
					Very valid reasons to disable JS exist, as it can be used to perform a lot of intrusive
					surveillance. CAPTCHAs - especially ReCAPTCHA - and ads are great examples of this.
					Uniquely identifying a device using only JS is <a
						href="https://coveryourtracks.eff.org/"
						target="_blank"
					>trivial</a> and they take advantage of that, so much so that they're able to track users
					without a single third party cookie.
				</p>
				<p>
					For some, it may also be important from a security standpoint, as JS is present in almost
					all common browser-based attacks. It's not really possible to defend against them without
					disabling it entirely.
				</p>
			</section>
			<section class="window margin-bottom">
				<h2 class="margin-bottom">
					Dedecentralization.
				</h2>
				<p class="margin-bottom">
					The Internet is a place decentralized by nature, or at least used to be. No single entity
					should ever hold the power to singlehandedly take down, perform editorial decisions on
					and surveil a major part of it. And yet, Cloudflare is in exactly that position. As we've
					seen with the <a
						href="https://blog.cloudflare.com/cloudflare-outage-on-july-17-2020/"
						target="_blank"
					>outages</a>, aforementioned attacks and <a
						href="https://www.wired.com/story/free-speech-issue-cloudflare/"
						target="_blank"
					>website takedowns</a>, that position can be a very dangerous one.
				</p>
				<p>
					There are companies like Google who have proven themselves to be much less responsible in
					the same position. But that position should remain empty, no matter how friendly the company
					who wants to fill it is.
				</p>
			</section>
			<h2 class="window margin-bottom">
				Alright. How to fix it?
			</h2>
			<section class="window margin-bottom">
				<h3 class="margin-bottom">
					Attacks.
				</h3>
				<p class="margin-bottom">
					It's a hard question. If your goal is to defend against the majority of attacks - small ones,
					with only a few IPs sending data at once - setting up a firewall is no big deal and will take
					care of most attacks you're at least slightly likely to encounter at some point.
				</p>
				<p>
					But if your small VPS is suddenly under attack from a major group with hundreds of gigabits'
					worth of bandwidth spread out across the globe directing all of it at you, it's going to be
					game over even with the most restrictive firewalls due to the sheer amount of data coming in.
					However, the chances of that happening are extremely slim - especially for the kinds of
					websites hosted on a small VPS - and giving up all of your users' privacy is a very high
					price for that small peace of mind.
				</p>
			</section>
			<section class="window margin-bottom">
				<h3 class="margin-bottom">
					TLS certificates.
				</h3>
				<p class="margin-bottom">
					The promise of something you would usually pay for being free is very enticing and some people
					may switch to Cloudflare just because of it. Unfortunately, they are only free in one sense of
					the word - price. Freedom is absent. As a free-tier user, you're not even allowed to download
					the generated certificate, let alone supply your own.
				</p>
				<p>
					Cloudflare isn't the only CA supplying free certificates, either. The <a
						href="https://www.abetterinternet.org/"
						target="_blank"
					>nonprofit-owned</a> <a
						href="https://letsencrypt.org/"
						target="_blank"
					>Let's encrypt</a> service has been operating for years and has always been free in both
					senses of the word.
				</p>
			</section>
			<section class="window margin-bottom">
				<h3 class="margin-bottom">
					Caching and the CDN.
				</h3>
				<p class="margin-bottom">
					Using a third party service to serve static content will save some bandwidth and possibly make
					your website load a little faster. But just like before, the numerous privacy implications
					greatly outweigh the cost of those minor improvements and give CDN providers a good amount
					of insight into who exactly uses your website.
				</p>
				<p class="margin-bottom">
					Most modern browsers are also able to cache resources on their own, if you ask them
					to and specify an amount of time to wait until they're fetched again. Granted, users are
					free to disable such caching or have browsers without it, but they're few and far
					between.
				</p>
				<p>
					Hosting content locally isn't always going to be the right answer and there are edge cases
					where using a CDN (perhaps even your own!) is genuinely the only option. As for the majority
					of websites, though, there's no need - especially if an effort is made not to use massive
					libraries and scripts just to spin a loading wheel.
				</p>
			</section>
		</main>
	</body>
</html>
